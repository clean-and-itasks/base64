# Changelog

#### 1.0.5

- Chore: support base `3.0`.

#### 1.0.4

- Fix: return error when invalid base64 string is provided to `decodeString`.

#### 1.0.3

- Chore: fix dependencies.

#### 1.0.2

- Chore: support base v2.0

#### 1.0.1

- Fix: skip all whitespaces when decoding, not only newlines.

## 1.0.0

- Initial version, contains an altered Text.Encodings.Base64 module currently present in Clean platform.
