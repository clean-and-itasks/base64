definition module Base64
/**
 * Base64 and Base64URL encoding/decoding according to RFC4648. More info:
 * - http://tools.ietf.org/html/rfc4648
 * - http://en.wikipedia.org/wiki/Base64
 *
 * @property-bootstrap
 *     import Data.Error
 *     import Gast.Gen
 *     import StdEnv
 *     import Text, Text.GenPrint
 */

from Data.Error import :: MaybeError

:: Length :== Int
:: Alphabet :== {#Char}
:: Offset :== Int
:: Padding :== Int

/**
 * Converts a String to a Base64-encoded `String`.
 *
 * @param The `String` to encode.
 * @result The encoded base64 `String`.
 * @property inverse: A.x :: String:
 *     base64Decode (base64Encode x) == Ok x
 */
base64Encode :: !.String -> .String

/**
 * Converts a `String` to a Base64-encoded `String` given a maximum line length.
 *
 * @param The `String` to encode.
 * @param The maximum line length, must be over 0 (abort otherwise).
 * @result The encoded base64 `String`.
 * @property inverse: A.len :: Int; x :: String:
 *     len > 0 ==> base64Decode (base64EncodeLen x len) == Ok x
 * @property max length: A.len :: Int; x :: String:
 *     len > 0 ==> all ((>=) len o size) (split "\n" (base64EncodeLen x len))
 */
base64EncodeLen :: !.String !Length -> .String

/**
 * Converts a `String` to an URL-safe Base64-encoded `String`.
 *
 * @param The `String` to encode.
 * @result The URL-safe encoded Base64 `String`.
 * @property inverse: A.x :: String:
 *     base64URLDecode (base64URLEncode x) == Ok x
 */
base64URLEncode :: !.String -> .String

/**
 * Converts a `String` to an URL-safe Base64-encoded `String` given a maximum line length.
 *
 * @param The `String` to encode.
 * @param The maxmimum line length, must be over 0 (abort otherwise).
 * @result The URL-safe encoded Base64 `String` on success, an error if the `String` could not be encoded.
 * @property inverse: A.len :: Int; x :: String:
 *     len > 0 ==> base64URLDecode (base64URLEncodeLen x len) == Ok x
 * @property max length: A.len :: Int; x :: String:
 *     len > 0 ==> all ((>=) len o size) (split "\n" (base64URLEncodeLen x len))
 */
base64URLEncodeLen :: !.String !Length -> .String

/**
 * Converts a Base64-encoded `String` to a `String`.
 *
 * @param The Base64-encoded `String`.
 * @result The decoded `String` if decoding was successful. Otherwise, an error.
 * @property works with space in input: A.x :: String:
 *     let str = base64Encode x in
 *     base64Decode (str % (0,size str / 2 - 1) +++ " " +++ str % (size str / 2, size str - 1)) == Ok x
 * @property does not yield run-time-error: A.str :: String:
 *     let decodedStr = base64Decode str in
 *     isOk decodedStr || isError decodedStr
 */
base64Decode :: !.String -> .MaybeError String .String

/**
 * Converts an URL-safe Base64-encoded `String` to a decoded `String`.
 *
 * @param The URL-safe Base64-encoded `String`.
 * @result The decoded `String` if decoding was successful. Otherwise, an error.
 */
base64URLDecode :: !.String -> .MaybeError String .String

/**
 * Decodes a Base64-encoded String in place.
 *
 * @param The Base64-encoded `String`.
 * @result The decoded `String` if decoding was successful. Otherwise, an error.
 * @property inverse: A.x :: String:
 *     base64DecodeUnique (base64Encode x) == Ok x
 * @property inverse with linebreaks: A.len :: Int; x :: String:
 *     len > 0 ==> base64DecodeUnique (base64EncodeLen x len) == Ok x
 */
base64DecodeUnique :: !*String -> MaybeError String .String

/**
 * Decodes a URL-safe Base64-encoded String in place.
 *
 * @param The URL-safe Base64-encoded `String`.
 * @result The decoded `String` if decoding was successful. Otherwise, an error.
 * @property inverse: A.x :: String:
 *     base64URLDecodeUnique (base64URLEncode x) == Ok x
 * @property inverse with linebreaks: A.len :: Int; x :: String:
 *     len > 0 ==> base64URLDecodeUnique (base64URLEncodeLen x len) == Ok x
 */
base64URLDecodeUnique :: !*String -> MaybeError String .String
